#include "game.h"
#include "assets.h"

//Constants for sprites
//todo change the numbers to the beginning of the sheet!
//! The GBC draws the sprite with the lowest sprite tile value on top. This will cause inconsistency if you're developing for both.
//* Player
const UINT8 PLAYER_UP_L = 0;
const UINT8 PLAYER_UP_R = 2;
const UINT8 PLAYER_MID_L = 4;
const UINT8 PLAYER_MID_R = 6;
const UINT8 PLAYER_DIAG_L = 8;
const UINT8 PLAYER_DIAG_R = 10;
const UINT8 PLAYER_DOWN_L = 12;
const UINT8 PLAYER_DOWN_R = 14;
//*Pipe head 1
const UINT8 PIPE_HEAD_1_L_L = 16;
const UINT8 PIPE_HEAD_1_L_R = 18;
const UINT8 PIPE_HEAD_1_R_L = 20;
const UINT8 PIPE_HEAD_1_R_R = 22;
//*Pipe head 2
const UINT8 PIPE_HEAD_2_L_L = 24;
const UINT8 PIPE_HEAD_2_L_R = 26;
const UINT8 PIPE_HEAD_2_R_L = 28;
const UINT8 PIPE_HEAD_2_R_R = 30;
//* Pipe body 1
const UINT8 PIPE_BODY_1_L_L = 32;
const UINT8 PIPE_BODY_1_L_R = 34;
const UINT8 PIPE_BODY_1_R_L = 36;
const UINT8 PIPE_BODY_1_R_R = 38;
//* Pipe body 2
const UINT8 PIPE_BODY_2_L_L = 40;
const UINT8 PIPE_BODY_2_L_R = 42;
const UINT8 PIPE_BODY_2_R_L = 44;
const UINT8 PIPE_BODY_2_R_R = 46;
//* numbers ///casi hackerrrr
const UINT8 NUM_0 = 66;
const UINT8 NUM_1 = 48;
const UINT8 NUM_2 = 50;
const UINT8 NUM_3 = 52;
const UINT8 NUM_4 = 54;
const UINT8 NUM_5 = 56;
const UINT8 NUM_6 = 58;
const UINT8 NUM_7 = 60;
const UINT8 NUM_8 = 62;
const UINT8 NUM_9 = 64;

UINT8 currentPipeFrame = 0;

struct FatSprite player;
struct ThiccSprite allPipes[8];
struct PipePair pairs[2];
enum GameState gameState = MENU;

INT8 player_speed = 0;
UBYTE gravity = 1;
UBYTE playerDilutor = 0;
BOOLEAN player_alive = TRUE;
BOOLEAN is_a_down = FALSE;
UINT32 score = 0;
UINT8 sprScore[3];
BOOLEAN secondCrashPlayed = FALSE;
BOOLEAN forceSecondNote = FALSE;

int main()
{
  // Init LCD
  LCDC_REG = 0x67;
  set_interrupts(VBL_IFLAG | LCD_IFLAG);
  STAT_REG = 0x45;

  // Set palettes
  BGP_REG = OBP0_REG = 0xE4U;
  OBP1_REG = 0xD2U;

  makeSprites();
  setupMenu();
  HIDE_SPRITES;

  DISPLAY_ON;

  // Position Window Layer
  WX_REG = 7;
  WY_REG = 120;

  SHOW_WIN;
  //HIDE_WIN;

  initSound();

  while (1)
  {
    game_loop();
  }
}

void game_loop()
{
  UBYTE joy;

  wait_vbl_done();

  joy = joypad();

  switch (gameState)
  {
  case MENU:
    if (joy & J_START)
    {
      setupPreGame();
      gameState = PRE_PLAY;
    }

    break;

  case PRE_PLAY:
    if (joy & J_A)
    {
      is_a_down = FALSE; //force a flap.
      playerInput(joy);
      gameState = PLAY;
    }
    break;

  case PLAY:
    //player_alive is actually more like "is this game?"
    if (player_alive)
    {
      if (playerDilutor == 0 || playerDilutor == 3 || playerDilutor == 6 || playerDilutor == 9)
        SCX_REG++;
      WX_REG--;
      if (WX_REG == 0)
        WX_REG = 7;
      playerInput(joy);
      animPlayer();
      spinPipes();
      movePlayer();
      move_pipe_pair(&pairs[0], pairs[0].x - 1);
      move_pipe_pair(&pairs[1], pairs[1].x - 1);
      if (pairs[1].scored == TRUE && score == 0)
      {
        move_thicc_sprite(&allPipes[4], allPipes[4].x, 0);
        move_thicc_sprite(&allPipes[5], allPipes[5].x, 0);
        move_thicc_sprite(&allPipes[6], allPipes[6].x, 0);
        move_thicc_sprite(&allPipes[7], allPipes[7].x, 0);
      }
      collide();
      showScore();
      if (forceSecondNote == TRUE)
        makeOtherNoise();
    }
    else
    {
      gameState = GAME_OVER;
      makeCrashNoise();
      //look down
      set_sprite_tile(0, PLAYER_DOWN_L);
      set_sprite_tile(1, PLAYER_DOWN_R);
    }

    break;

  case GAME_OVER:
    //do a dive.
    if (player.y < 144 - 20)
    {
      if (player_speed > 3)
      {
        move_fat_sprite(&player, player.x, player.y + player_speed);
      }
      else
      {
        move_fat_sprite(&player, player.x, player.y + 3);
      }
    }
    else if (secondCrashPlayed == FALSE)
    {
      secondCrashPlayed = TRUE;
      makeCrashNoise();
      move_fat_sprite(&player, player.x, 144 - 20);
      setupGameOver();
    }
    else if (joy & J_START)
    {
      secondCrashPlayed = FALSE;
      setupPreGame();
      gameState = PRE_PLAY;
    }
    break;

  default:
    break;
  }
}

void showScore()
{
  UINT8 auxScore = score;
  set_sprite_tile(sprScore[0], intToSpr(auxScore % 10));
  set_sprite_tile(sprScore[1], intToSpr((auxScore % 100) / 10));
  set_sprite_tile(sprScore[2], intToSpr(auxScore / 100));

  if (auxScore < 10)
  {
    move_sprite(sprScore[0], 84, 16);
    move_sprite(sprScore[1], 0, 0);
    move_sprite(sprScore[2], 0, 0);
  }
  else if (auxScore < 100)
  {
    move_sprite(sprScore[0], 84 + 5, 16);
    move_sprite(sprScore[1], 84 - 5, 16);
    move_sprite(sprScore[2], 0, 0);
  }
  else
  {
    move_sprite(sprScore[0], 84 + 5 + 8, 16);
    move_sprite(sprScore[1], 84, 16);
    move_sprite(sprScore[2], 84 - 5 - 8, 16);
  }
}

void centerScore()
{
  UINT8 auxScore = score;
  set_sprite_tile(sprScore[0], intToSpr(auxScore % 10));
  set_sprite_tile(sprScore[1], intToSpr((auxScore % 100) / 10));
  set_sprite_tile(sprScore[2], intToSpr(auxScore / 100));

  if (auxScore < 10)
  {
    move_sprite(sprScore[0], 84, 80);
    move_sprite(sprScore[1], 0, 0);
    move_sprite(sprScore[2], 0, 0);
  }
  else if (auxScore < 100)
  {
    move_sprite(sprScore[0], 84 + 5, 80);
    move_sprite(sprScore[1], 84 - 5, 80);
    move_sprite(sprScore[2], 0, 0);
  }
  else
  {
    move_sprite(sprScore[0], 84 + 5 + 8, 80);
    move_sprite(sprScore[1], 84, 80);
    move_sprite(sprScore[2], 84 - 5 - 8, 80);
  }
}

UINT8 intToSpr(UINT8 i)
{
  switch (i)
  {
  case 0:
    return NUM_0;
  case 1:
    return NUM_1;
  case 2:
    return NUM_2;
  case 3:
    return NUM_3;
  case 4:
    return NUM_4;
  case 5:
    return NUM_5;
  case 6:
    return NUM_6;
  case 7:
    return NUM_7;
  case 8:
    return NUM_8;
  case 9:
    return NUM_9;
  default:
    return NUM_0;
  }
}

void playerInput(UBYTE joy)
{
  if (joy & J_A && is_a_down == FALSE)
  {
    is_a_down = TRUE;
    makeNoise();
    player_speed = -3;
    playerDilutor = 0;
  }
  if (!(joy & J_A))
  {
    is_a_down = FALSE;
  }
}

void movePlayer()
{
  if (playerDilutor++ == 0)
  {
    //Do gravity
    player_speed += gravity;
  }
  else
  {
    //dilute time
    if (playerDilutor == 10)
      playerDilutor = 0;
  }

  //update the position
  move_fat_sprite(&player, player.x, player.y + player_speed);
}

void animPlayer()
{
  if (player_speed < 0)
  {
    //look up
    set_sprite_tile(0, PLAYER_UP_L);
    set_sprite_tile(1, PLAYER_UP_R);
  }
  else if (player_speed > 1)
  {
    //look down
    set_sprite_tile(0, PLAYER_DOWN_L);
    set_sprite_tile(1, PLAYER_DOWN_R);
  }
  else if (player_speed > 0)
  {
    //look down
    set_sprite_tile(0, PLAYER_DIAG_L);
    set_sprite_tile(1, PLAYER_DIAG_R);
  }
  else
  {
    //look mid
    set_sprite_tile(0, PLAYER_MID_L);
    set_sprite_tile(1, PLAYER_MID_R);
  }
}

void spinPipes()
{
  currentPipeFrame = currentPipeFrame % 12;

  switch (currentPipeFrame)
  {
  //case 0:
  case 1:
    set_sprite_tile(pairs[0].heads[0]->sprites[0], PIPE_HEAD_1_L_L);
    set_sprite_tile(pairs[0].heads[0]->sprites[1], PIPE_HEAD_1_L_R);
    set_sprite_tile(pairs[0].heads[0]->sprites[2], PIPE_HEAD_1_R_L);
    set_sprite_tile(pairs[0].heads[0]->sprites[3], PIPE_HEAD_1_R_R);

    set_sprite_prop(pairs[0].heads[0]->sprites[0], get_sprite_prop(pairs[0].heads[0]->sprites[0]) & ~S_FLIPX);
    set_sprite_prop(pairs[0].heads[0]->sprites[1], get_sprite_prop(pairs[0].heads[0]->sprites[1]) & ~S_FLIPX);
    set_sprite_prop(pairs[0].heads[0]->sprites[2], get_sprite_prop(pairs[0].heads[0]->sprites[2]) & ~S_FLIPX);
    set_sprite_prop(pairs[0].heads[0]->sprites[3], get_sprite_prop(pairs[0].heads[0]->sprites[3]) & ~S_FLIPX);

    set_sprite_tile(pairs[0].heads[1]->sprites[0], PIPE_HEAD_1_L_L);
    set_sprite_tile(pairs[0].heads[1]->sprites[1], PIPE_HEAD_1_L_R);
    set_sprite_tile(pairs[0].heads[1]->sprites[2], PIPE_HEAD_1_R_L);
    set_sprite_tile(pairs[0].heads[1]->sprites[3], PIPE_HEAD_1_R_R);

    set_sprite_prop(pairs[0].heads[1]->sprites[0], get_sprite_prop(pairs[0].heads[1]->sprites[0]) & ~S_FLIPX);
    set_sprite_prop(pairs[0].heads[1]->sprites[1], get_sprite_prop(pairs[0].heads[1]->sprites[1]) & ~S_FLIPX);
    set_sprite_prop(pairs[0].heads[1]->sprites[2], get_sprite_prop(pairs[0].heads[1]->sprites[2]) & ~S_FLIPX);
    set_sprite_prop(pairs[0].heads[1]->sprites[3], get_sprite_prop(pairs[0].heads[1]->sprites[3]) & ~S_FLIPX);
    break;
  //case 2:
  case 3:
    set_sprite_tile(pairs[1].heads[0]->sprites[0], PIPE_HEAD_1_L_L);
    set_sprite_tile(pairs[1].heads[0]->sprites[1], PIPE_HEAD_1_L_R);
    set_sprite_tile(pairs[1].heads[0]->sprites[2], PIPE_HEAD_1_R_L);
    set_sprite_tile(pairs[1].heads[0]->sprites[3], PIPE_HEAD_1_R_R);

    set_sprite_prop(pairs[1].heads[0]->sprites[0], get_sprite_prop(pairs[1].heads[0]->sprites[0]) & ~S_FLIPX);
    set_sprite_prop(pairs[1].heads[0]->sprites[1], get_sprite_prop(pairs[1].heads[0]->sprites[1]) & ~S_FLIPX);
    set_sprite_prop(pairs[1].heads[0]->sprites[2], get_sprite_prop(pairs[1].heads[0]->sprites[2]) & ~S_FLIPX);
    set_sprite_prop(pairs[1].heads[0]->sprites[3], get_sprite_prop(pairs[1].heads[0]->sprites[3]) & ~S_FLIPX);

    set_sprite_tile(pairs[1].heads[1]->sprites[0], PIPE_HEAD_1_L_L);
    set_sprite_tile(pairs[1].heads[1]->sprites[1], PIPE_HEAD_1_L_R);
    set_sprite_tile(pairs[1].heads[1]->sprites[2], PIPE_HEAD_1_R_L);
    set_sprite_tile(pairs[1].heads[1]->sprites[3], PIPE_HEAD_1_R_R);

    set_sprite_prop(pairs[1].heads[1]->sprites[0], get_sprite_prop(pairs[1].heads[1]->sprites[0]) & ~S_FLIPX);
    set_sprite_prop(pairs[1].heads[1]->sprites[1], get_sprite_prop(pairs[1].heads[1]->sprites[1]) & ~S_FLIPX);
    set_sprite_prop(pairs[1].heads[1]->sprites[2], get_sprite_prop(pairs[1].heads[1]->sprites[2]) & ~S_FLIPX);
    set_sprite_prop(pairs[1].heads[1]->sprites[3], get_sprite_prop(pairs[1].heads[1]->sprites[3]) & ~S_FLIPX);
    break;

  //case 4:
  case 5:
    set_sprite_tile(pairs[0].heads[0]->sprites[0], PIPE_HEAD_2_L_L);
    set_sprite_tile(pairs[0].heads[0]->sprites[1], PIPE_HEAD_2_L_R);
    set_sprite_tile(pairs[0].heads[0]->sprites[2], PIPE_HEAD_2_R_L);
    set_sprite_tile(pairs[0].heads[0]->sprites[3], PIPE_HEAD_2_R_R);

    set_sprite_prop(pairs[0].heads[0]->sprites[0], get_sprite_prop(pairs[0].heads[0]->sprites[0]) & ~S_FLIPX);
    set_sprite_prop(pairs[0].heads[0]->sprites[1], get_sprite_prop(pairs[0].heads[0]->sprites[1]) & ~S_FLIPX);
    set_sprite_prop(pairs[0].heads[0]->sprites[2], get_sprite_prop(pairs[0].heads[0]->sprites[2]) & ~S_FLIPX);
    set_sprite_prop(pairs[0].heads[0]->sprites[3], get_sprite_prop(pairs[0].heads[0]->sprites[3]) & ~S_FLIPX);

    set_sprite_tile(pairs[0].heads[1]->sprites[0], PIPE_HEAD_2_L_L);
    set_sprite_tile(pairs[0].heads[1]->sprites[1], PIPE_HEAD_2_L_R);
    set_sprite_tile(pairs[0].heads[1]->sprites[2], PIPE_HEAD_2_R_L);
    set_sprite_tile(pairs[0].heads[1]->sprites[3], PIPE_HEAD_2_R_R);

    set_sprite_prop(pairs[0].heads[1]->sprites[0], get_sprite_prop(pairs[0].heads[1]->sprites[0]) & ~S_FLIPX);
    set_sprite_prop(pairs[0].heads[1]->sprites[1], get_sprite_prop(pairs[0].heads[1]->sprites[1]) & ~S_FLIPX);
    set_sprite_prop(pairs[0].heads[1]->sprites[2], get_sprite_prop(pairs[0].heads[1]->sprites[2]) & ~S_FLIPX);
    set_sprite_prop(pairs[0].heads[1]->sprites[3], get_sprite_prop(pairs[0].heads[1]->sprites[3]) & ~S_FLIPX);

    break;
  //case 6:
  case 7:

    set_sprite_tile(pairs[1].heads[0]->sprites[0], PIPE_HEAD_2_L_L);
    set_sprite_tile(pairs[1].heads[0]->sprites[1], PIPE_HEAD_2_L_R);
    set_sprite_tile(pairs[1].heads[0]->sprites[2], PIPE_HEAD_2_R_L);
    set_sprite_tile(pairs[1].heads[0]->sprites[3], PIPE_HEAD_2_R_R);

    set_sprite_prop(pairs[1].heads[0]->sprites[0], get_sprite_prop(pairs[1].heads[0]->sprites[0]) & ~S_FLIPX);
    set_sprite_prop(pairs[1].heads[0]->sprites[1], get_sprite_prop(pairs[1].heads[0]->sprites[1]) & ~S_FLIPX);
    set_sprite_prop(pairs[1].heads[0]->sprites[2], get_sprite_prop(pairs[1].heads[0]->sprites[2]) & ~S_FLIPX);
    set_sprite_prop(pairs[1].heads[0]->sprites[3], get_sprite_prop(pairs[1].heads[0]->sprites[3]) & ~S_FLIPX);

    set_sprite_tile(pairs[1].heads[1]->sprites[0], PIPE_HEAD_2_L_L);
    set_sprite_tile(pairs[1].heads[1]->sprites[1], PIPE_HEAD_2_L_R);
    set_sprite_tile(pairs[1].heads[1]->sprites[2], PIPE_HEAD_2_R_L);
    set_sprite_tile(pairs[1].heads[1]->sprites[3], PIPE_HEAD_2_R_R);

    set_sprite_prop(pairs[1].heads[1]->sprites[0], get_sprite_prop(pairs[1].heads[1]->sprites[0]) & ~S_FLIPX);
    set_sprite_prop(pairs[1].heads[1]->sprites[1], get_sprite_prop(pairs[1].heads[1]->sprites[1]) & ~S_FLIPX);
    set_sprite_prop(pairs[1].heads[1]->sprites[2], get_sprite_prop(pairs[1].heads[1]->sprites[2]) & ~S_FLIPX);
    set_sprite_prop(pairs[1].heads[1]->sprites[3], get_sprite_prop(pairs[1].heads[1]->sprites[3]) & ~S_FLIPX);
    break;

  //case 8:
  case 9:
    set_sprite_tile(pairs[0].heads[0]->sprites[3], PIPE_HEAD_2_L_L);
    set_sprite_tile(pairs[0].heads[0]->sprites[2], PIPE_HEAD_2_L_R);
    set_sprite_tile(pairs[0].heads[0]->sprites[1], PIPE_HEAD_2_R_L);
    set_sprite_tile(pairs[0].heads[0]->sprites[0], PIPE_HEAD_2_R_R);

    set_sprite_prop(pairs[0].heads[0]->sprites[0], get_sprite_prop(pairs[0].heads[0]->sprites[0]) | S_FLIPX);
    set_sprite_prop(pairs[0].heads[0]->sprites[1], get_sprite_prop(pairs[0].heads[0]->sprites[1]) | S_FLIPX);
    set_sprite_prop(pairs[0].heads[0]->sprites[2], get_sprite_prop(pairs[0].heads[0]->sprites[2]) | S_FLIPX);
    set_sprite_prop(pairs[0].heads[0]->sprites[3], get_sprite_prop(pairs[0].heads[0]->sprites[3]) | S_FLIPX);

    set_sprite_tile(pairs[0].heads[1]->sprites[3], PIPE_HEAD_2_L_L);
    set_sprite_tile(pairs[0].heads[1]->sprites[2], PIPE_HEAD_2_L_R);
    set_sprite_tile(pairs[0].heads[1]->sprites[1], PIPE_HEAD_2_R_L);
    set_sprite_tile(pairs[0].heads[1]->sprites[0], PIPE_HEAD_2_R_R);

    set_sprite_prop(pairs[0].heads[1]->sprites[0], get_sprite_prop(pairs[0].heads[1]->sprites[0]) | S_FLIPX);
    set_sprite_prop(pairs[0].heads[1]->sprites[1], get_sprite_prop(pairs[0].heads[1]->sprites[1]) | S_FLIPX);
    set_sprite_prop(pairs[0].heads[1]->sprites[2], get_sprite_prop(pairs[0].heads[1]->sprites[2]) | S_FLIPX);
    set_sprite_prop(pairs[0].heads[1]->sprites[3], get_sprite_prop(pairs[0].heads[1]->sprites[3]) | S_FLIPX);
    break;
  //case 10:
  case 11:
    set_sprite_tile(pairs[1].heads[0]->sprites[3], PIPE_HEAD_2_L_L);
    set_sprite_tile(pairs[1].heads[0]->sprites[2], PIPE_HEAD_2_L_R);
    set_sprite_tile(pairs[1].heads[0]->sprites[1], PIPE_HEAD_2_R_L);
    set_sprite_tile(pairs[1].heads[0]->sprites[0], PIPE_HEAD_2_R_R);

    set_sprite_prop(pairs[1].heads[0]->sprites[0], get_sprite_prop(pairs[1].heads[0]->sprites[0]) | S_FLIPX);
    set_sprite_prop(pairs[1].heads[0]->sprites[1], get_sprite_prop(pairs[1].heads[0]->sprites[1]) | S_FLIPX);
    set_sprite_prop(pairs[1].heads[0]->sprites[2], get_sprite_prop(pairs[1].heads[0]->sprites[2]) | S_FLIPX);
    set_sprite_prop(pairs[1].heads[0]->sprites[3], get_sprite_prop(pairs[1].heads[0]->sprites[3]) | S_FLIPX);

    set_sprite_tile(pairs[1].heads[1]->sprites[3], PIPE_HEAD_2_L_L);
    set_sprite_tile(pairs[1].heads[1]->sprites[2], PIPE_HEAD_2_L_R);
    set_sprite_tile(pairs[1].heads[1]->sprites[1], PIPE_HEAD_2_R_L);
    set_sprite_tile(pairs[1].heads[1]->sprites[0], PIPE_HEAD_2_R_R);

    set_sprite_prop(pairs[1].heads[1]->sprites[0], get_sprite_prop(pairs[1].heads[1]->sprites[0]) | S_FLIPX);
    set_sprite_prop(pairs[1].heads[1]->sprites[1], get_sprite_prop(pairs[1].heads[1]->sprites[1]) | S_FLIPX);
    set_sprite_prop(pairs[1].heads[1]->sprites[2], get_sprite_prop(pairs[1].heads[1]->sprites[2]) | S_FLIPX);
    set_sprite_prop(pairs[1].heads[1]->sprites[3], get_sprite_prop(pairs[1].heads[1]->sprites[3]) | S_FLIPX);
    break;

  default:
    break;
  }

  currentPipeFrame++;
}

void move_fat_sprite(struct FatSprite *s, UINT8 nx, UINT8 ny)
{
  s->x = nx;
  s->y = ny;
  move_sprite(s->left, s->x, s->y);
  move_sprite(s->right, s->x + 8, s->y);
}

void move_thicc_sprite(struct ThiccSprite *s, UINT8 nx, UINT8 ny)
{
  s->x = nx;
  s->y = ny;
  move_sprite(s->sprites[0], s->x, s->y);
  move_sprite(s->sprites[1], s->x + 8, s->y);
  move_sprite(s->sprites[2], s->x + 16, s->y);
  move_sprite(s->sprites[3], s->x + 24, s->y);
}

void move_pipe_pair(struct PipePair *pp, UINT8 nx)
{
  pp->x = nx;

  if (nx > 168 && nx < 255 - 16 * 3 && pp->scored == TRUE)
    randomize_pipe_pair(pp);

  move_thicc_sprite(pp->bodys[0], nx, pp->bodys[0]->y);
  move_thicc_sprite(pp->bodys[1], nx, pp->bodys[1]->y);

  move_thicc_sprite(pp->heads[0], nx, pp->heads[0]->y);
  move_thicc_sprite(pp->heads[1], nx, pp->heads[1]->y);
}

void randomize_pipe_pair(struct PipePair *pp)
{

  INT8 luckyNumber;
  UINT8 s1 = 0;
  UINT8 s2 = 0;

  do
  {
    INT8 r = rand();
    luckyNumber = r % 3;
    if (luckyNumber < 0)
      luckyNumber *= -1;
  } while (luckyNumber == pp->kind);
  pp->x = 168;
  pp->scored = FALSE; //reset the scored flag.

  switch (luckyNumber)
  {
  case 0:
    pp->kind = UP;
    move_thicc_sprite(pp->heads[0], 168, 16);

    move_thicc_sprite(pp->heads[1], 168, 16 + 72);
    move_thicc_sprite(pp->bodys[0], 168, 16 + 72 + 16);
    move_thicc_sprite(pp->bodys[1], 168, 16 + 72 + 32);

    for (s1 = 0; s1 < 4; s1++)
    {
      set_sprite_prop(pp->bodys[0]->sprites[s1], get_sprite_prop(pp->bodys[0]->sprites[s1]) & ~S_FLIPY);
      set_sprite_prop(pp->bodys[1]->sprites[s1], get_sprite_prop(pp->bodys[1]->sprites[s1]) & ~S_FLIPY);
    }

    break;
  case 1:
    pp->kind = MID;
    move_thicc_sprite(pp->bodys[0], 168, 16);
    move_thicc_sprite(pp->heads[0], 168, 32);

    move_thicc_sprite(pp->heads[1], 168, 32 + 72);
    move_thicc_sprite(pp->bodys[1], 168, 32 + 72 + 16);

    for (s1 = 0; s1 < 4; s1++)
    {
      set_sprite_prop(pp->bodys[1]->sprites[s1], get_sprite_prop(pp->bodys[1]->sprites[s1]) & ~S_FLIPY);
    }
    for (s2 = 0; s2 < 4; s2++)
    {
      set_sprite_prop(pp->bodys[0]->sprites[s2], get_sprite_prop(pp->bodys[0]->sprites[s2]) | S_FLIPY);
    }

    break;
  case 2:
    pp->kind = DOWN;
    move_thicc_sprite(pp->bodys[0], 168, 16);
    move_thicc_sprite(pp->bodys[1], 168, 32);
    move_thicc_sprite(pp->heads[0], 168, 48);

    move_thicc_sprite(pp->heads[1], 168, 48 + 72);

    for (s2 = 0; s2 < 4; s2++)
    {
      set_sprite_prop(pp->bodys[0]->sprites[s2], get_sprite_prop(pp->bodys[0]->sprites[s2]) | S_FLIPY);
      set_sprite_prop(pp->bodys[1]->sprites[s2], get_sprite_prop(pp->bodys[1]->sprites[s2]) | S_FLIPY);
    }

    break;

  default: //this shouldn't happen, right?
    break;
  }
}

void collide()
{
  UINT8 i = 0;

  UBYTE x1 = player.x - 8;
  UBYTE y1 = player.y - 16;
  UBYTE w1 = 16;
  UBYTE h1 = 16;
  UBYTE w2 = 32;
  UBYTE h2 = 16;

  for (i = 0; i < 8; i++)
  {
    UBYTE x2 = allPipes[i].x - 8;
    UBYTE y2 = allPipes[i].y - 16;
    if (!(x1 + w1 < x2 || x2 + w2 < x1 || y1 + h1 < y2 || y2 + h2 < y1))
    {
      player_alive = FALSE;
    }
  }

  if (player.y < 0 || player.y > 144 - 16)
  {
    player_alive = FALSE;
  }

  if (pairs[0].scored == FALSE && pairs[0].x < player.x)
  {
    pairs[0].scored = TRUE;
    score++;
    makeOtherNoise();
  }
  if (pairs[1].scored == FALSE && pairs[1].x < player.x)
  {
    pairs[1].scored = TRUE;
    score++;
    makeOtherNoise();
  }
}

void makeSprites()
{
  // Initialize the background graphics
  set_bkg_data(0, 0, tileset);
  set_bkg_tiles(0, 0, 32, 32, screen_tiles);
  set_win_tiles(0, 0, 32, 3, window_tiles);

  // Initialize the sprite graphics
  set_sprite_data(0, 88, sprites);

  //making player sprite
  set_sprite_tile(0, PLAYER_MID_L);
  set_sprite_tile(1, PLAYER_MID_R);
  player.left = 0;
  player.right = 1;

  //lets make 1 pipe.
  //2 body
  set_sprite_tile(2, PIPE_BODY_1_L_L);
  set_sprite_tile(3, PIPE_BODY_1_L_R);
  set_sprite_tile(4, PIPE_BODY_1_R_L);
  set_sprite_tile(5, PIPE_BODY_1_R_R);
  allPipes[0].sprites[0] = 2;
  allPipes[0].sprites[1] = 3;
  allPipes[0].sprites[2] = 4;
  allPipes[0].sprites[3] = 5;
  set_sprite_tile(6, PIPE_BODY_1_L_L);
  set_sprite_tile(7, PIPE_BODY_1_L_R);
  set_sprite_tile(8, PIPE_BODY_1_R_L);
  set_sprite_tile(9, PIPE_BODY_1_R_R);
  allPipes[1].sprites[0] = 6;
  allPipes[1].sprites[1] = 7;
  allPipes[1].sprites[2] = 8;
  allPipes[1].sprites[3] = 9;
  //2 heads
  set_sprite_tile(10, PIPE_HEAD_1_L_L);
  set_sprite_tile(11, PIPE_HEAD_1_L_R);
  set_sprite_tile(12, PIPE_HEAD_1_R_L);
  set_sprite_tile(13, PIPE_HEAD_1_R_R);
  set_sprite_prop(10, S_FLIPY);
  set_sprite_prop(11, S_FLIPY);
  set_sprite_prop(12, S_FLIPY);
  set_sprite_prop(13, S_FLIPY);
  allPipes[2].sprites[0] = 10;
  allPipes[2].sprites[1] = 11,
  allPipes[2].sprites[2] = 12;
  allPipes[2].sprites[3] = 13;
  set_sprite_tile(14, PIPE_HEAD_1_L_L);
  set_sprite_tile(15, PIPE_HEAD_1_L_R);
  set_sprite_tile(16, PIPE_HEAD_1_R_L);
  set_sprite_tile(17, PIPE_HEAD_1_R_R);
  allPipes[3].sprites[0] = 14;
  allPipes[3].sprites[1] = 15,
  allPipes[3].sprites[2] = 16;
  allPipes[3].sprites[3] = 17;

  pairs[0].bodys[0] = &allPipes[0];
  pairs[0].bodys[1] = &allPipes[1];
  pairs[0].heads[0] = &allPipes[2];
  pairs[0].heads[1] = &allPipes[3];

  //lets make 2 pipe.
  //2 body
  set_sprite_tile(18, PIPE_BODY_1_L_L);
  set_sprite_tile(19, PIPE_BODY_1_L_R);
  set_sprite_tile(20, PIPE_BODY_1_R_L);
  set_sprite_tile(21, PIPE_BODY_1_R_R);
  allPipes[4].sprites[0] = 18;
  allPipes[4].sprites[1] = 19;
  allPipes[4].sprites[2] = 20;
  allPipes[4].sprites[3] = 21;
  set_sprite_tile(22, PIPE_BODY_1_L_L);
  set_sprite_tile(23, PIPE_BODY_1_L_R);
  set_sprite_tile(24, PIPE_BODY_1_R_L);
  set_sprite_tile(25, PIPE_BODY_1_R_R);
  allPipes[5].sprites[0] = 22;
  allPipes[5].sprites[1] = 23;
  allPipes[5].sprites[2] = 24;
  allPipes[5].sprites[3] = 25;
  //2 heads
  set_sprite_tile(26, PIPE_HEAD_1_L_L);
  set_sprite_tile(27, PIPE_HEAD_1_L_R);
  set_sprite_tile(28, PIPE_HEAD_1_R_L);
  set_sprite_tile(29, PIPE_HEAD_1_R_R);
  set_sprite_prop(26, S_FLIPY);
  set_sprite_prop(27, S_FLIPY);
  set_sprite_prop(28, S_FLIPY);
  set_sprite_prop(29, S_FLIPY);
  allPipes[6].sprites[0] = 26;
  allPipes[6].sprites[1] = 27,
  allPipes[6].sprites[2] = 28;
  allPipes[6].sprites[3] = 29;
  set_sprite_tile(30, PIPE_HEAD_1_L_L);
  set_sprite_tile(31, PIPE_HEAD_1_L_R);
  set_sprite_tile(32, PIPE_HEAD_1_R_L);
  set_sprite_tile(33, PIPE_HEAD_1_R_R);
  allPipes[7].sprites[0] = 30;
  allPipes[7].sprites[1] = 31,
  allPipes[7].sprites[2] = 32;
  allPipes[7].sprites[3] = 33;

  pairs[1].bodys[0] = &allPipes[4];
  pairs[1].bodys[1] = &allPipes[5];
  pairs[1].heads[0] = &allPipes[6];
  pairs[1].heads[1] = &allPipes[7];

  //make the score
  sprScore[0] = 34;
  sprScore[1] = 35;
  sprScore[2] = 36;
  set_sprite_tile(sprScore[0], NUM_0);
  set_sprite_tile(sprScore[1], NUM_0);
  set_sprite_tile(sprScore[2], NUM_0);
}

void setupPreGame()
{
  SHOW_SPRITES;
  SCY_REG = 0;
  //SCX_REG = 0;

  /* We use the DIV register to get a random initial seed */
  initrand(DIV_REG);
  move_fat_sprite(&player, 70, 60);
  randomize_pipe_pair(&pairs[0]);
  randomize_pipe_pair(&pairs[1]);
  move_pipe_pair(&pairs[1], 60);
  pairs[1].scored = TRUE; //start at true to not trigger an extra point.

  //the "for" statement sometimes fail to compile. dunno why.
  move_thicc_sprite(&allPipes[4], allPipes[4].x, 0);
  move_thicc_sprite(&allPipes[5], allPipes[5].x, 0);
  move_thicc_sprite(&allPipes[6], allPipes[6].x, 0);
  move_thicc_sprite(&allPipes[7], allPipes[7].x, 0);

  player_alive = TRUE;
  //look mid
  set_sprite_tile(0, PLAYER_MID_L);
  set_sprite_tile(1, PLAYER_MID_R);
  score = 0;
  showScore();
}

void setupMenu()
{
  SCY_REG = 120;
  SCX_REG = 112;
}
void setupGameOver()
{
  SCY_REG = 120;
  SCX_REG = -16;
  //hide pipes
  randomize_pipe_pair(&pairs[0]);
  randomize_pipe_pair(&pairs[1]);
  centerScore();
}

void initSound()
{
  NR52_REG = 0xf8u;
  NR51_REG = 0x00u;
  NR50_REG = 0x77U;
}

void makeNoise()
{
  NR10_REG = 0x34U;
  NR11_REG = 0x70u;
  NR12_REG = 0xf0u;
  NR13_REG = 0x0au;
  NR14_REG = 0xc6u;
  NR51_REG |= 0x11;
}

void makeOtherNoise()
{
  //NR20_REG = 0x34U;
  // NR21_REG = 0x80U;
  // NR22_REG = 0xF0U;
  // NR23_REG = 0x0FU;
  // NR24_REG = 0xffU;
  // NR51_REG |= 0x22;

  if (NR52_REG & 0x02)
  {
    return;
  }
  else if (forceSecondNote == TRUE)
  {
    //segunda nota
    forceSecondNote = FALSE;
    NR21_REG = 0x99U;
    NR22_REG = 0x68U;
    NR23_REG = 0xDBU;
    NR24_REG = 0xC7U;
    NR51_REG |= 0x22;
  }
  else
  {
    forceSecondNote = TRUE;
    // primera nota.
    NR21_REG = 0xAEU;
    NR22_REG = 0x73U;
    NR23_REG = 0x9FU;
    NR24_REG = 0xC7U;
    NR51_REG |= 0x22;
  }
}

void makeCrashNoise()
{
  NR41_REG = 0x30U;
  NR42_REG = 0x70U;
  NR43_REG = 0x3fU;
  NR44_REG = 0xc6U;
  NR51_REG |= 0x88;
}