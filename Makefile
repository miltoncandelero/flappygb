#GBDK_PATH_UNIX := $(subst ',,$(subst \,/,'$(GBDK_PATH)'))

CC = lcc -v -Wa-l -Wl-m -Wl-j -Wl-yt1 -Iinclude -Igenerated

ROM_BUILD_DIR = build/gb

OBJDIR = obj

all:

$(OBJDIR)/%.o:	src/%.c
	$(CC) -c -o $@ $<

$(OBJDIR)/%.o:	src/data/%.c
	$(CC) -c -o $@ $<	

$(OBJDIR)/%.s:	%.c
	$(CC) -S -o $@ $<

$(OBJDIR)/%.o:	%.s
	$(CC) -c -o $@ $<

$(ROM_BUILD_DIR)/%.gb:	$(OBJDIR)/%.o
	mkdir -p $(ROM_BUILD_DIR)
	$(CC) -Wa-l -Wl-m -Wl-j -DUSE_SFR_FOR_REG -Wl-yt1 -Wl-yo32 -Wl-ya4 -o $@ $^	

clean:
	rm -f obj/*
	rm -rf build

rom: $(ROM_BUILD_DIR)/game.gb
