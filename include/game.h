#ifndef GAME_H
#define GAME_H

#include <gb/gb.h>
#include <rand.h>

#include <stdio.h>
#include <stdarg.h>
#include <string.h>

struct FatSprite
{
  UBYTE x;
  UBYTE y;
  UINT8 left;
  UINT8 right;
};

struct ThiccSprite
{
  UBYTE x;
  UBYTE y;
  UINT8 sprites[4];
};

enum PipeKind
{
  UP,
  MID,
  DOWN
};

struct PipePair
{
  UINT8 x;
  enum PipeKind kind;
  struct ThiccSprite *bodys[2];
  struct ThiccSprite *heads[2];
  BOOLEAN scored;
};

enum GameState
{
  MENU,
  PRE_PLAY,
  PLAY,
  GAME_OVER
};

void game_loop();
void initSound();
void makeNoise();
void makeOtherNoise();
void movePlayer();
void animPlayer();
void playerInput(UBYTE joy);
void move_fat_sprite(struct FatSprite *s, UINT8 nx, UINT8 ny);
void move_thicc_sprite(struct ThiccSprite *s, UINT8 nx, UINT8 ny);
void move_pipe_pair(struct PipePair *pp, UINT8 nx);
void randomize_pipe_pair(struct PipePair *pp);
void collide();
void makeSprites();
void setupPreGame();
void setupMenu();
void setupGameOver();
void showScore();
UINT8 intToSpr(UINT8 i);
void spinPipes();
void makeCrashNoise();
void centerScore();

#endif
